import uvicorn
from fastapi import FastAPI
from api.base import router as base_router


class Config:
    DEFAULT_TIMEOUT = 20
    TAGS_METADATA = [
        {
            "name": "json_thro_actions",
            "description": "Pull file to some DBMS"
        },
    ]
    BASIC_RESPONSES = {
        200: {
            'description': 'OK'
        },
        503: {
            'description': 'Kafka server error'
        },
        400: {
            'description': 'Input error: incorrect json'
        },
        500: {
            'description': 'Unknown server error'
        },
    }

app = FastAPI(
    openapi_tags=Config.TAGS_METADATA,
    docs_url="/docs",
    redoc_url=None,
    title="Interview_fasapi+redis",
    version="dev",
    contact={
        "name": "no_one",
        "url": "https://nowhere.com/",
    },
)
app.include_router(base_router)


if __name__=="__main__":
    uvicorn.run(app, host="0.0.0.0", port=8000)

