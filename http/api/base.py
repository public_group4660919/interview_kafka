import json
from fastapi import (
    APIRouter, 
    HTTPException, 
    Response
)
from kafka.errors import KafkaError
from utils.adapters import kafka_sync_send, producer


router = APIRouter()


@router.post(
    '/json_pull',
    tags=["json_thro_actions"],
)
def json_pull(data: str) -> Response:
    try:
        kafka_sync_send(producer, data)
        return {"msg": "json was pulled to broker"}
    except KafkaError:
        raise HTTPException(status_code=503, detail='broker server error (kafka)')
    except json.JSONDecodeError:
        raise HTTPException(status_code=400, detail='incorrect json input')
    except Exception as e:
        raise HTTPException(status_code=500, detail=f'Internal server error: {e}')
