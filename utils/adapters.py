import json
import redis
from configparser import ConfigParser
from kafka import KafkaProducer


def read_config(section, key):
    config = ConfigParser()
    config.read('./utils/config.ini')
    return config[section][key]


def redis_sock():
    return redis.Redis(
        host=read_config('NETWORK', 'HOST_IP'),
        port=read_config('NETWORK', 'REDIS_PORT'), 
        password=read_config('DATABASE', 'REDIS_PSWD'),
        )


producer = KafkaProducer(
    bootstrap_servers=[
        read_config('NETWORK', 'HOST_IP') + ':'
        + str(read_config('NETWORK', 'KAFKA_PORT'))
        ],
    value_serializer=lambda m: json.dumps(m).encode('ascii'),
    retries=3,
    )


def kafka_sync_send(producer: KafkaProducer, data: str) -> int:
    msg = json.loads(data)
    producer.send('interview-topic', msg)
    return 0

