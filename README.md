# BUILD AND RUN PROJECT

- set your host IP to ./config.ini
- execute:
    $ docker-compose --env-file ./config.ini up -d
- open swagger and play with /json-pull api:
    http://localhost:8080
